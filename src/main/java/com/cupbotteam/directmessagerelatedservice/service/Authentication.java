package com.cupbotteam.directmessagerelatedservice.service;

import java.time.LocalDateTime;

/**
 * An interface to Authentication, use to handle Challange-Response Check
 * authentication between HTTP request.
 */
public interface Authentication {
    /**
     * Generate a challange token, and saves it.
     *
     * @return String of 10 characters that would be use as the challange token.
     */
    public String generateCrcToken();

    /**
     * Authenticate response token by running through every challange token
     * that has been generated.
     *
     * @param responseToken , response token to be authenticate.
     * @return true if the response token pass the authentication,
     * false if the response token failed.
     */
    public boolean authenticateResponseToken(String responseToken);

    /**
     * Check's through every challange token that has been generated,
     * and delete token's that have expired. Currently, every token has
     * and life span of 1 hour.
     */
    public void checkOutdateTokens();

    public void addGeneratedTokens(String token, LocalDateTime time);

    public boolean removeGeneratedTokens(String token);

    public boolean isTokenStillValid(String token);
}

