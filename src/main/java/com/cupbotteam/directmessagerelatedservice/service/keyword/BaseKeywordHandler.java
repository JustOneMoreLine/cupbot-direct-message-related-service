package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;

public abstract class BaseKeywordHandler implements KeywordHandler {
    protected KeywordHandler next;
    protected KeywordHandler endHandler;

    // set next handler
    @Override
    public void setNextHandler(KeywordHandler nextHandler) {
        this.next = nextHandler;
    }

    @Override
    public void handleRequest(DirectMessageJson message) {

        if (handle(message)) {
            //do nothing
        } else {
            if (next != null) {
                next.handleRequest(message);
            } else {
                endHandler.handleRequest(message);
            }
        }
    }

    public abstract boolean handle(DirectMessageJson message);
}
