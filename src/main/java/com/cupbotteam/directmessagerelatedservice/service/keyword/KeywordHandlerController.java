package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;

/**
 * An interface use to initiate and controll handler's chain of responsibility.
 */
public interface KeywordHandlerController {

    public void handle(DirectMessageJson message);
}
