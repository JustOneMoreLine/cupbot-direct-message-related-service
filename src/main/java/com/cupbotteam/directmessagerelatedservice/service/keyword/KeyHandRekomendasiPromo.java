package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafePromoCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KeyHandRekomendasiPromo extends BaseKeywordHandler {
    CafePromoCode cafePromoCode;

    @Autowired
    public KeyHandRekomendasiPromo(CafePromoCode cafePromoCode) {
        this.cafePromoCode = cafePromoCode;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            long ownerId = message.getSenderID();
            cafePromoCode.rekomendasiPromo(ownerId);
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(17, text.length()));
        if (keyword.equals("!rekomendasiPromo")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() == 17) {
            return true;
        }
        return false;
    }
}
