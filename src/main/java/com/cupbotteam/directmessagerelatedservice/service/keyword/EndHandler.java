package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;

public class EndHandler extends BaseKeywordHandler {

    @Override
    public void setNextHandler(KeywordHandler nextHandler) {
        // do nothing
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        // do nothing
        return true;
    }
}
