package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeAllTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AllTimeHandler extends BaseKeywordHandler {
    CafeAllTime allTime;

    @Autowired
    public AllTimeHandler(CafeAllTime allTime) {
        this.allTime = allTime;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message)) {
            long requesterId = message.getSenderID();
            allTime.showAllTime(requesterId);
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(9, text.length()));
        return keyword.equalsIgnoreCase("!allTime ");
    }
}
