package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;

/**
 * An interface shared by all handlers.
 */
public interface KeywordHandler {

    /**
     * Set the next handler between all the handlers.
     *
     * @param nextHandler , the next handler for this handler.
     */
    public void setNextHandler(KeywordHandler nextHandler);

    /**
     * The main method every handler have, to read direct message
     * and check's if they can handle it or not.
     *
     * @param message , direct message that needs to be handle.
     */
    // message akan mengandung seluruh string dari dm
    public void handleRequest(DirectMessageJson message);

}
