package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KeyHandShowMyRegisterCafe extends BaseKeywordHandler {
    CafeRegistration cafeRegistration;

    @Autowired
    public KeyHandShowMyRegisterCafe(CafeRegistration cafeRegistration) {
        this.cafeRegistration = cafeRegistration;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            long ownerId = message.getSenderID();
            cafeRegistration.showMyCafe(ownerId);
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(11, text.length()));
        if (keyword.equals("!showMyCafe")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() == 11) {
            return true;
        }
        return false;
    }
}
