package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.PromoDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafePromoCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class KeyHandRegisPromo extends BaseKeywordHandler {
    CafePromoCode cafePromoCode;
    PromoDatabaseAdapter promoDatabaseAdapter;


    @Autowired
    public KeyHandRegisPromo(CafePromoCode cafePromoCode) {
        this.cafePromoCode = cafePromoCode;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            long newId = createId(message);
            CafeId newCafeId = createCafeId(message);
            String newPromoCode = createPromoCode(message);
            LocalDateTime newStartOfPromo = createStartOfPromo(message);
            LocalDateTime newEndOfPromo = createEndOfPromo(message);
            String newPromoDescription = createPromoDescription(message);
            cafePromoCode.regisPromo(newId, newCafeId, newPromoCode,
                    newStartOfPromo, newEndOfPromo, newPromoDescription);
            return true;
        } else {
            System.out.println("masih salah");
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(12, text.length()));
        if (keyword.equals("!regisPromo ")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() < 12) {
            return false; // no content, not even for keyword
        }
        String content = text.substring(12, text.length());
        String[] contentSplit = content.split("/");
        if (contentSplit.length == 5) {
            return true;
        }
        return false;
    }

    private long createId(DirectMessageJson message) {
        long id = message.getSenderID();
        return id;
    }

    private String createPromoCode(DirectMessageJson message) {
        String promoCode = getPromoCodeFrom(message);
        return promoCode;
    }

    private LocalDateTime createStartOfPromo(DirectMessageJson message) {
        LocalDateTime startOfPromo = getStartOfPromo(message);
        return startOfPromo;
    }

    private LocalDateTime createEndOfPromo(DirectMessageJson message) {
        LocalDateTime endOfPromo = getEndOfPromo(message);
        return endOfPromo;
    }

    private String createPromoDescription(DirectMessageJson message) {
        String description = getDescriptionFrom(message);
        return description;
    }

    private CafeId createCafeId(DirectMessageJson message) {
        CafeId cafeId = new CafeId();
        cafeId.setCafeName(getCafeNameFrom(message));
        cafeId.setCafeOwnerTwitterId(message.getSenderID());
        return cafeId;
    }

    private String[] contentSplit(String message) {
        String removedKeywordFromText = message.substring(12, message.length());
        return removedKeywordFromText.split("/");
    }

    private String getCafeNameFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[0];
    }

    private String getPromoCodeFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[1];
    }

    private LocalDateTime getStartOfPromo(DirectMessageJson message) {
        String date = contentSplit(message.getText())[2] + " 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        return dateTime;
    }

    private LocalDateTime getEndOfPromo(DirectMessageJson message) {
        String date = contentSplit(message.getText())[3] + " 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        return dateTime;
    }

    private String getDescriptionFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[4];
    }
}
