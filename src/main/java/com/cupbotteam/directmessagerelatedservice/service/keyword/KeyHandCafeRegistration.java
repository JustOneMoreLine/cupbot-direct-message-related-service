package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KeyHandCafeRegistration extends BaseKeywordHandler {
    CafeRegistration cafeRegistration;

    @Autowired
    public KeyHandCafeRegistration(CafeRegistration cafeRegistration) {
        this.cafeRegistration = cafeRegistration;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            CafeId newCafeId = createCafeId(message);
            CafeAddress newCafeAddress = createCafeAddress(message);
            cafeRegistration.registerCafe(newCafeId, newCafeAddress);
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(14, text.length()));
        if (keyword.equals("!cafeRegister ")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() < 14) {
            return false; // no content, not even for keyword
        }
        String content = text.substring(14, text.length());
        String[] contentSplit = content.split("/");
        if (contentSplit.length == 6) {
            return true;
        }
        return false;
    }

    private CafeId createCafeId(DirectMessageJson message) {
        CafeId cafeId = new CafeId();
        cafeId.setCafeName(getCafeNameFrom(message));
        cafeId.setCafeOwnerTwitterId(message.getSenderID());
        return cafeId;
    }

    private CafeAddress createCafeAddress(DirectMessageJson message) {
        CafeAddress cafeAddress = new CafeAddress();
        cafeAddress.setStreet(getStreetFrom(message));
        cafeAddress.setSubDistrict(getSubDistrictFrom(message));
        cafeAddress.setDistrict(getDistrictFrom(message));
        cafeAddress.setCity(getCityFrom(message));
        cafeAddress.setProvince(getProvinceFrom(message));
        return cafeAddress;
    }

    private String[] contentSplit(String message) {
        String removedKeywordFromText = message.substring(14, message.length());
        return removedKeywordFromText.split("/");
    }

    private String getCafeNameFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[0];
    }

    private String getStreetFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[1];
    }

    private String getSubDistrictFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[2];
    }

    private String getDistrictFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[3];
    }

    private String getCityFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[4];
    }

    private String getProvinceFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[5];
    }
}
