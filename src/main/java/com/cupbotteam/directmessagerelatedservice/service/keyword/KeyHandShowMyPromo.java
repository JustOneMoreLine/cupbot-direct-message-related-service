package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafePromoCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KeyHandShowMyPromo extends BaseKeywordHandler {
    CafePromoCode cafePromoCode;

    @Autowired
    public KeyHandShowMyPromo(CafePromoCode cafePromoCode) {
        this.cafePromoCode = cafePromoCode;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            System.out.println("masuk ke showmypromo kok");
            long ownerId = message.getSenderID();
            cafePromoCode.showMyPromo(ownerId);
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(12, text.length()));
        if (keyword.equals("!showMyPromo")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() == 12) {
            return true;
        }
        return false;
    }
}
