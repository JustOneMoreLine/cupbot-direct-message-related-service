package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KeyHandCafeUnregister extends BaseKeywordHandler {
    CafeRegistration cafeRegistration;

    @Autowired
    public KeyHandCafeUnregister(CafeRegistration cafeRegistration) {
        this.cafeRegistration = cafeRegistration;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            CafeId cafeId = new CafeId();
            cafeId.setCafeName(getCafeNameFrom(message));
            cafeId.setCafeOwnerTwitterId(message.getSenderID());
            cafeRegistration.unregisterCafe(cafeId);
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(16, text.length()));
        if (keyword.equals("!cafeUnregister ")) {
            return true;
        }
        return false;
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() < 16) {
            return false; // no content, not even for keyword

        }
        String content = text.substring(16, text.length());
        String[] contentSplit = content.split("/");
        if (contentSplit.length == 1) {
            return true;
        }
        return false;
    }

    private String[] contentSplit(String message) {
        String removedKeywordFromText = message.substring(16, message.length());
        return removedKeywordFromText.split("/");
    }

    private String getCafeNameFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[0];
    }
}
