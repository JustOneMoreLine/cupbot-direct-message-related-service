package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeNearby;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NearbyHandler extends BaseKeywordHandler {
    CafeNearby nearby;

    @Autowired
    public NearbyHandler(CafeNearby nearby) {
        this.nearby = nearby;
        this.endHandler = new EndHandler();
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        if (keywordIsCorrectFrom(message) && allRegistrationParamFilledFrom(message)) {
            String region = getRegionFrom(message);
            String city = getCityFrom(message);
            String province = getProvinceFrom(message);
            long requesterId = message.getSenderID();
            nearby.showNearby(requesterId, region, city, province);
            return true;
        } else {
            return false;
        }
    }

    private boolean keywordIsCorrectFrom(DirectMessageJson message) {
        String text = message.getText();
        String keyword = text.substring(0, Math.min(8, text.length()));
        return keyword.equalsIgnoreCase("!nearby ");
    }

    private boolean allRegistrationParamFilledFrom(DirectMessageJson message) {
        String text = message.getText();
        if (text.length() < 8) {
            return false;
        }
        String content = text.substring(8, text.length());
        String[] contentSplit = content.split("/");
        return contentSplit.length == 3;
    }

    private String[] contentSplit(String message) {
        String removedKeywordFromText = message.substring(8, message.length());
        return removedKeywordFromText.split("/");
    }

    private String getRegionFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[0];
    }

    private String getCityFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[1];
    }

    private String getProvinceFrom(DirectMessageJson message) {
        return contentSplit(message.getText())[2];
    }
}
