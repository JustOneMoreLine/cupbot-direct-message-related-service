package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Implementation of KeywordHandlerController,
 * use to initiate and controll handler's chain of responsibility.
 */
@Service
public class KeywordHandlerControllerImpl implements KeywordHandlerController {
    // notice if the chain is set up or not.
    boolean isItNotSetUp = true;
    // The first handler to start the chain
    KeywordHandler firstHandler;
    // List of handlers, will be set up by Spring's Autowired
    private List<KeywordHandler> allHandler;

    @Autowired
    public KeywordHandlerControllerImpl(List<KeywordHandler> allHandler) {
        this.allHandler = allHandler;
    }

    /**
     * Runs the first handler to start the chain of responsibility.
     * If the chain has not been set up, set's it up first before running the chain.
     *
     * @param message , direct message to handle
     */
    @Override
    public void handle(DirectMessageJson message) {
        if (isItNotSetUp) {
            setUp();
        }
        firstHandler.handleRequest(message);
    }

    /**
     * Sets a chain of responsibility from the list of available
     * handlers.
     */
    public void setUp() {
        if (allHandler.size() > 1) {
            for (int i = 0; i < allHandler.size() - 1; i++) {
                KeywordHandler aHandler = allHandler.get(i);
                KeywordHandler nextHandler = allHandler.get(i + 1);
                aHandler.setNextHandler(nextHandler);
            }
        }
        firstHandler = allHandler.get(0);
        isItNotSetUp = false;
    }
}
