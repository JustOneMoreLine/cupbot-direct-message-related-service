package com.cupbotteam.directmessagerelatedservice.service;

import com.cupbotteam.directmessagerelatedservice.utility.CrcTokenGenerator;
import com.cupbotteam.directmessagerelatedservice.utility.Sha256;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;


@Service
/**
 * An implementation of Authentication,
 * use to handle Challange-Response Check authentication
 * between HTTP request.
 */
public class AuthenticationImpl implements Authentication {
    private HashMap<String, LocalDateTime> generatedTokens = new HashMap<String, LocalDateTime>();

    /**
     * Generate a challange token, and saves it.
     *
     * @return String of 10 characters that would be use as the challange token.
     */
    @Override
    public String generateCrcToken() {
        String crcToken = CrcTokenGenerator.generateCrcToken();
        if (generatedTokens.containsKey(crcToken)) {
            return generateCrcToken();
        }
        generatedTokens.put(crcToken, LocalDateTime.now());
        return crcToken;
    }

    /**
     * Authenticate response token by running through every challange token
     * that has been generated.
     *
     * @param responseToken , response token to be authenticate.
     * @return true if the response token pass the authentication,
     * false if the response token failed.
     */
    @Override
    public boolean authenticateResponseToken(String responseToken) {
        for (String token : generatedTokens.keySet()) {
            if (responseToken.equals(Sha256.hash(token))) {
                generatedTokens.remove(token);
                return true;
            }
        }
        return false;
    }

    /**
     * Check's through every challange token that has been generated,
     * and delete token's that have expired. Currently, every token has
     * and life span of 1 hour.
     */
    @Override
    public void checkOutdateTokens() {
        LocalDateTime now = LocalDateTime.now();
        for (String token : generatedTokens.keySet()) {
            LocalDateTime tokenCreationDate = generatedTokens.get(token);
            if (tokenCreationDate.until(now, ChronoUnit.HOURS) > 1) {
                generatedTokens.remove(token);
            }
        }
    }

    @Override
    public void addGeneratedTokens(String token, LocalDateTime time) {
        generatedTokens.put(token, time);
    }

    @Override
    public boolean removeGeneratedTokens(String token) {
        if (generatedTokens.containsKey(token)) {
            generatedTokens.remove(token);
            return true;
        }
        return false;
    }

    @Override
    public boolean isTokenStillValid(String token) {
        if (generatedTokens.containsKey(token)) {
            return true;
        }
        return false;
    }
}
