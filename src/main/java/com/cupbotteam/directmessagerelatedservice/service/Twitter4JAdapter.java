package com.cupbotteam.directmessagerelatedservice.service;

import twitter4j.DirectMessage;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import java.util.Map;

/**
 * An interface for Twitter4JAdapter,
 * use as an adapter between services and Twitter4J.
 */
public interface Twitter4JAdapter {

    public Status createFavorite(long id);

    public Status destroyFavorite(long id);

    public Status retweetStatus(long statusId);

    public Status unRetweetStatus(long statusId);

    public Status showStatus(long id);

    public Status destroyStatus(long id);

    public void updateStatus(String status);

    public DirectMessage showDirectMessage(long id);

    public DirectMessage destroyDirectMessage(long id);

    public void sendDirectMessage(long recipientId, String text);

    public Map<String, RateLimitStatus> getRateLimitStatus()
            ;
}
