package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import com.cupbotteam.directmessagerelatedservice.service.CafeDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class CafeAllTimeImpl implements CafeAllTime {
    private Comparator<Cafe> sortByRank;
    CafeDatabaseAdapter cafeDatabaseAdapter;
    Twitter4JAdapter twitter;

    @Autowired
    public CafeAllTimeImpl(Twitter4JAdapter twitter, CafeDatabaseAdapter cafeDatabaseAdapter) {
        this.twitter = twitter;
        this.cafeDatabaseAdapter = cafeDatabaseAdapter;
        this.sortByRank = new SortByRank();
    }

    @Override
    public void showAllTime(long twitterId) {
        System.out.println("showAllTime is called");
        List<Cafe> cafesAllTime = findAllTime();
        replyAllTime(twitterId, cafesAllTime);
    }

    @Override
    public List<Cafe> findAllTime() {
        System.out.println("findAllTime is called");
        List<Cafe> alltimeCafes = new ArrayList<Cafe>();
        Cafe[] allCafes = cafeDatabaseAdapter.getAllCafe();
        for (Cafe cf : allCafes) {
            alltimeCafes.add(cf);
        }
        alltimeCafes.sort(sortByRank);
        if (alltimeCafes.size() <= 10) {
            return alltimeCafes;
        } else {    
            return alltimeCafes.subList(0, 10);
        }
    }

    private void replyAllTime(long twitterId, List<Cafe> results) {
        String text = "Here is out top 10 cafe of all time:\n";
        for (Cafe cafe : results) {
            text = text.concat(
                    "\n" + cafe.getId().getCafeName() + "\n"
                    + "Address: " + cafe.getCafeAddress().toString() + "\n"
                    + "Cafe Rank: " + cafe.getCafeOfAllTimeRank() + "\n"
                    + "Cafe of The Week Wins: " + cafe.getCafeOfTheWeekWins() + "\n"
            );
        }
        twitter.sendDirectMessage(twitterId, text);
    }

    class SortByRank implements Comparator<Cafe> {
         @Override
        public int compare(Cafe cf1, Cafe cf2) {
            return (int) (cf1.getCafeOfAllTimeRank() - cf2.getCafeOfAllTimeRank());
        }
    }
}
