package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import java.util.List;

public interface CafeAllTime {
    public void showAllTime(long twitterId);
    public List<Cafe> findAllTime();
}
