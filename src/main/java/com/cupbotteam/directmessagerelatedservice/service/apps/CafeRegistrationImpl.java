package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.service.CafeDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Service
public class CafeRegistrationImpl implements CafeRegistration {
    CafeDatabaseAdapter cafeDatabaseAdapter;
    Twitter4JAdapter twitter;
    private Comparator<Cafe> sortRankByCafeWins;

    @Autowired
    public CafeRegistrationImpl(Twitter4JAdapter twitter, CafeDatabaseAdapter cafeDatabaseAdapter) {
        this.twitter = twitter;
        this.cafeDatabaseAdapter = cafeDatabaseAdapter;
        this.sortRankByCafeWins = new SortByRank();
    }

    @Override
    public void registerCafe(CafeId cafeId, CafeAddress cafeAddress) {
        if (doesThisCafeExist(cafeId)) {
            replyToSender(cafeId.getCafeOwnerTwitterId(), cafeId.getCafeName());
        } else {
            Cafe cafe = createCafe(cafeId, cafeAddress);
            if (cafeIsSaved(cafe)) {
                replySaveSuccess(cafeId.getCafeOwnerTwitterId(), cafe);
            } else {
                replySaveFail(cafeId.getCafeOwnerTwitterId());
            }
        }
    }

    @Override
    public void unregisterCafe(CafeId cafeId) {
        System.out.println("unregister called");
        if (doesThisCafeExist(cafeId)) {
            if (removeCafe(cafeId)) {
                replyRemoveSuccess(cafeId);
            } else {
                replyRemoveFail(cafeId.getCafeOwnerTwitterId());
            }
        } else {
            replyCafeDontExist(cafeId);
        }
    }

    @Override
    public void showMyCafe (long ownerId) {
        System.out.println("showmycafe called");
        if (noCafeOwnedByThis(ownerId)) {
            replyNoCafeRegistered(ownerId);
        } else {
            // continue through noCafeOwnedByThis
        }
    }

    private boolean noCafeOwnedByThis(long ownerId) {
        Cafe[] allCafeOwned = cafeDatabaseAdapter.getCafeByOwnerTwitterId(ownerId);
        if (allCafeOwned.length == 0) {
            return true;
        }
        replyAllCafeByThis(ownerId, allCafeOwned);
        return false;
    }

    private void replyNoCafeRegistered(long ownerId) {
        String text = "No cafe is registered under this twitter account.";
        twitter.sendDirectMessage(ownerId, text);
    }

    private void replyAllCafeByThis(long ownerId, Cafe[] allRegisteredCafes) {
        String text = "My Cafe(s):\n";
        for (Cafe cafe : allRegisteredCafes) {
            text = text.concat(
                    "\n" + cafe.getId().getCafeName() + "\n"
                    + "Cafe Address: " + cafe.getCafeAddress().toString() + "\n"
                    + "Cafe Rank: " + cafe.getCafeOfAllTimeRank() + "\n"
                    + "Cafe of The Week Wins: " + cafe.getCafeOfTheWeekWins()
                    );
        }
        twitter.sendDirectMessage(ownerId, text);
    }

    private boolean removeCafe(CafeId cafeId) {
        Cafe[] deletedCafe = cafeDatabaseAdapter.deleteCafe(cafeId);
        if (deletedCafe.length == 0) {
            return false;
        }
        if (cafeId.equals(deletedCafe[0].getId())) {
            return true;
        }
        return false;
    }

    private void replyRemoveSuccess(CafeId cafeId) {
        String text = cafeId.getCafeName() + " have been unregistered. It's sad to see you go :(";
        twitter.sendDirectMessage(cafeId.getCafeOwnerTwitterId(), text);
    }

    private void replyRemoveFail(long twitterId) {
        String text = "Sorry! Something wrong happen in our end, please try again later.";
        twitter.sendDirectMessage(twitterId, text);
    }

    private void replyCafeDontExist(CafeId cafeId) {
        String text = cafeId.getCafeName() + " is not registered.";
        twitter.sendDirectMessage(cafeId.getCafeOwnerTwitterId(), text);
    }

    private void replySaveSuccess(long twitterId, Cafe savedCafe) {
        String cafeName = savedCafe.getId().getCafeName();
        String text = cafeName + " have been successfully added! Welcome to the club!";
        twitter.sendDirectMessage(twitterId, text);
    }

    private void replySaveFail(long twitterId) {
        String text = "Sorry! Something went wrong in our end, please try again later.";
        twitter.sendDirectMessage(twitterId, text);
    }

    private boolean cafeIsSaved(Cafe cafe) {
        Cafe savedCafe = cafeDatabaseAdapter.addCafe(cafe);
        if (savedCafe == null || !(cafe.equals(savedCafe))) {
            return false;
        }
        return true;
    }

    private Cafe createCafe(CafeId cafeId, CafeAddress cafeAddress) {
        Cafe registerCafe = new Cafe();
        registerCafe.setId(cafeId);
        registerCafe.setCafeAddress(cafeAddress);
        registerCafe.setCafeOfTheWeekWins(0);
        long rank = getLowestRank();
        registerCafe.setCafeOfAllTimeRank(rank);
        return registerCafe;
    }

    private void replyToSender(long twitterId, String cafeName) {
        String text = "I'am sorry, but \"" + cafeName + "\" has already been registered";
        twitter.sendDirectMessage(twitterId, text);
    }

    private boolean doesThisCafeExist(CafeId cafeId) {
        Cafe[] cafe = cafeDatabaseAdapter.getCafe(cafeId);
        if (cafe.length == 0) {
            return false;
        }
        return true;
    }

    private long getLowestRank() {
        Cafe[] listOfAllCafe = cafeDatabaseAdapter.getAllCafe();
        if (listOfAllCafe.length == 0) {
            return 1;
        }
        List<Cafe> list = Arrays.asList(listOfAllCafe);
        list.sort(sortRankByCafeWins);
        Cafe lastCafe = list.get(list.size() - 1);
        return lastCafe.getCafeOfAllTimeRank() + 1;
    }

    class SortByRank implements Comparator<Cafe> {
        @Override
        public int compare(Cafe o1, Cafe o2) {
            return (int) (o1.getCafeOfAllTimeRank() - o2.getCafeOfAllTimeRank());
        }
    }
}
