package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;

public interface CafeRegistration {

    public void registerCafe(CafeId cafeId, CafeAddress cafeAddress);

    public void unregisterCafe(CafeId cafeId);

    public void showMyCafe(long ownerId);
}
