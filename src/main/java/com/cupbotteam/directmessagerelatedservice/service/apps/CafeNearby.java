package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import java.util.List;

public interface CafeNearby {
    void showNearby(long twitterId, String region, String city, String province);
    List<Cafe> findNearby(String region, String city, String province);
}

