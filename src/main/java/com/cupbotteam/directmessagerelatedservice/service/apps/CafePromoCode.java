package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import java.time.LocalDateTime;

public interface CafePromoCode {
    public void showMyPromo(long twitterId);

    public void regisPromo(long id,
                           CafeId cafeId,
                           String promoCode,
                           LocalDateTime startOfPromo,
                           LocalDateTime endOfPromo,
                           String promoDescription);

    public void rekomendasiPromo(long twitterId);
}

