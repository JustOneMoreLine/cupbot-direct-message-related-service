package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.CafePromo;
import com.cupbotteam.directmessagerelatedservice.service.PromoDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CafePromoCodeImpl implements CafePromoCode {
    PromoDatabaseAdapter promoDatabaseAdapter;
    Twitter4JAdapter twitter;

    @Autowired
    public CafePromoCodeImpl(Twitter4JAdapter twitter, PromoDatabaseAdapter promoDatabaseAdapter) {
        this.twitter = twitter;
        this.promoDatabaseAdapter = promoDatabaseAdapter;
    }

    @Override
    public void showMyPromo(long twitterId) {
        System.out.println("showCafeWithPromo called");
        if (noPromoOwnedByThis(twitterId)) {
            replyNoPromoRegistered(twitterId);
        }
    }

    private boolean noPromoOwnedByThis(long twitterId) {
        System.out.println("ini id nya = " + twitterId);
        CafePromo[] promoRegistered = promoDatabaseAdapter.getAllPromo();
        List<CafePromo> promoDimiliki = new ArrayList<CafePromo>();
        System.out.println("terpanggil ke sini");
        if (promoRegistered.length == 0) {
            return true;
        } else {
            for (CafePromo code : promoRegistered) {
                if (code.getCafeId().getCafeOwnerTwitterId() == twitterId) {
                    promoDimiliki.add(code);
                }
            }
        }
        replyPromoByThis(twitterId, promoDimiliki);
        return false;
    }

    private void replyPromoByThis(long twitterId, List<CafePromo> promoRegistered) {
        System.out.println("harusnya udah dibales bener");
        String text = "Promo(s):\n";
        for (CafePromo cafe : promoRegistered) {
            text = text.concat(
                    "\n" + cafe.getCafeId().getCafeName() + "\n"
                            + "ID Promo : " + cafe.getId() + "\n"
                            + "Cafe Promo Code : " + cafe.getPromoCode() + "\n"
                            + "Deskripsi: " + cafe.getPromoDescription() + "\n"
            );
        }
        twitter.sendDirectMessage(twitterId, text);

    }

    private void replyNoPromoRegistered(long twitterId) {
        String text = "No Promo is registered under this twitter account.";
        twitter.sendDirectMessage(twitterId, text);
    }


    @Override
    public void regisPromo(long id,
                           CafeId cafeId,
                           String promoCode,
                           LocalDateTime startOfPromo,
                           LocalDateTime endOfPromo,
                           String promoDescription) {
        System.out.println("regisPromo terpanggil");
        if (doesThisPromoExist(id)) {
            System.out.println("Masuk ke tahap 1");
            replyToSender(cafeId.getCafeOwnerTwitterId(), cafeId.getCafeName());
        } else {
            System.out.println("Masuk ke tahap 2");
            CafePromo cafe = createPromo(id, cafeId, promoCode, startOfPromo, endOfPromo, promoDescription);
            if (cafeIsSaved(cafe)) {
                System.out.println("Harusnya Balesan 1");
                replySaveSuccess(cafeId.getCafeOwnerTwitterId(), cafe);
            } else {
                System.out.println("Harusnya Balesan 2");
                replySaveFail(cafeId.getCafeOwnerTwitterId());
            }
        }
    }

    private void replySaveSuccess(long twitterId, CafePromo savedCafe) {
        long promoCode = savedCafe.getId();
        String text = promoCode + " have been successfully added! Welcome to the club!";
        twitter.sendDirectMessage(twitterId, text);
    }

    private void replySaveFail(long twitterId) {
        String text = "Sorry! Something went wrong in our end, please try again later.";
        twitter.sendDirectMessage(twitterId, text);
    }

    private boolean cafeIsSaved(CafePromo promo) {
        CafePromo savedCafe = null;
        try {
            savedCafe = promoDatabaseAdapter.addPromo(promo);
        } catch (Exception e) {
            return false;
        }
        if (savedCafe == null) {
            return false;
        }
        return true;
    }

    private CafePromo createPromo(long id,
                             CafeId cafeId,
                             String promoCode,
                             LocalDateTime startOfPromo,
                             LocalDateTime endOfPromo,
                             String promoDescription) {
        System.out.println("Masuk ke Create Promo");
        CafePromo registerPromo = new CafePromo();
        registerPromo.setId(id);
        registerPromo.setCafeId(cafeId);
        registerPromo.setPromoCode(promoCode);
        registerPromo.setStartOfPromo(startOfPromo);
        registerPromo.setEndOfPromo(endOfPromo);
        registerPromo.setPromoDescription(promoDescription);
        return registerPromo;
    }

    private void replyToSender(long twitterId, String cafeName) {
        String text = "I'am sorry, but \"" + cafeName + "\" has already been registered";
        twitter.sendDirectMessage(twitterId, text);
    }

    private boolean doesThisPromoExist(long id) {
        CafePromo[] cafePromo = promoDatabaseAdapter.getPromoById(id);
        if (cafePromo.length == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void rekomendasiPromo(long twitterId) {
        List<CafePromo> promoNow = pilihRekomendasi();
        if (promoNow == null) {
            System.out.println("masuk ke rek1");
            replyNoPromoNow(twitterId);
        } else {
            replyPromoNow(twitterId, promoNow);
        }
    }

    private void replyNoPromoNow(long twitterId) {
        String text = "Maaf saat ini sedang tidak ada promo yang berlaku.";
        twitter.sendDirectMessage(twitterId, text);
    }

    private void replyPromoNow(long twitterId, List<CafePromo> promoNow) {
        String text = "Promo(s):\n";
        for (CafePromo cafe : promoNow) {
            System.out.println(cafe);
            text = text.concat(
                    "\n" + cafe.getCafeId().getCafeName() + "\n"
                            + "ID Promo : " + cafe.getId() + "\n"
                            + "Cafe Promo Code : " + cafe.getPromoCode() + "\n"
                            + "Deskripsi: " + cafe.getPromoDescription() + "\n"
            );
        }
        twitter.sendDirectMessage(twitterId, text);
    }

    private List<CafePromo> pilihRekomendasi() {
        LocalDateTime now = LocalDateTime.now();
        CafePromo[] promo = promoDatabaseAdapter.getAllPromo();
        List<CafePromo> promoBerlaku = new ArrayList<CafePromo>();
        if (promo.length == 0) {
            return null;
        } else {
            for (CafePromo code : promo) {
                LocalDateTime akhirMasa = code.getEndOfPromo();
                boolean cekMasaBerlaku = now.isBefore(akhirMasa);
                if (cekMasaBerlaku) {
                    promoBerlaku.add(code);
                }
            }
            return promoBerlaku;
        }
    }



}
