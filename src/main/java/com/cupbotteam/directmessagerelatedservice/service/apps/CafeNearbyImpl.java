package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.service.CafeDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class CafeNearbyImpl implements CafeNearby {
    private Comparator<Cafe> sortByRank;
    CafeDatabaseAdapter cafeDatabaseAdapter;
    Twitter4JAdapter twitter;

    @Autowired
    public CafeNearbyImpl(Twitter4JAdapter twitter, CafeDatabaseAdapter cafeDatabaseAdapter) {
        this.twitter = twitter;
        this.cafeDatabaseAdapter = cafeDatabaseAdapter;
        this.sortByRank = new SortByRank();
    }

    @Override
    public void showNearby(long twitterId, String region, String city, String province) {
        System.out.println("showNearby is called");
        if (cafesDontExist(region, city, province)) {
            replyNoCafeNearby(twitterId);
        } else {
            List<Cafe> cafesNearby = chooseBestFive(region, city, province);
            replyCafesExist(twitterId, cafesNearby);
        }
    }

    @Override
    public List<Cafe> findNearby(String region, String city, String province) {
        System.out.println("findNearby is called");
        String reqAddress = region + ", " + city + ", " + province;
        return getClosest(reqAddress);
    }

    public List<Cafe> getClosest(String address) {
        List<Cafe> closestCafes = new ArrayList<Cafe>();
        Cafe[] allCafes = cafeDatabaseAdapter.getAllCafe();
        for (Cafe cf : allCafes) {
            CafeAddress cfAddress = cf.getCafeAddress();
            if ((cfAddress.toString()).toLowerCase().contains(address.toLowerCase())) {
                closestCafes.add(cf);
            }
        }
        closestCafes.sort(sortByRank);
        return closestCafes;
    }

    private boolean cafesDontExist(String region, String city, String province) {
        List<Cafe> fullResults = findNearby(region, city, province);
        return fullResults.size() == 0;
    }

    private List<Cafe> chooseBestFive(String region, String city, String province) {
        List<Cafe> results = findNearby(region, city, province);
        if (results.size() <= 5) {
            return results;
        } else {
            return results.subList(0, 5);
        }
    }

    private void replyNoCafeNearby(long twitterId) {
        String text = "Sorry, we could not find any cafes anywhere near your location :(";
        twitter.sendDirectMessage(twitterId, text);
    }

    private void replyCafesExist(long twitterId, List<Cafe> results) {
        String text = "We have found the best cafes in your area! Here are they:\n";
        for (Cafe cafe : results) {
            text = text.concat(
                    "\n" + cafe.getId().getCafeName() + "\n"
                    + "Address: " + cafe.getCafeAddress().toString() + "\n"
                    + "Cafe Rank: " + cafe.getCafeOfAllTimeRank() + "\n"
                    + "Cafe of The Week Wins: " + cafe.getCafeOfTheWeekWins() + "\n"
            );
        }
        System.out.println(text);
        twitter.sendDirectMessage(twitterId, text);
    }

    class SortByRank implements Comparator<Cafe> {
        @Override
        public int compare(Cafe cf1, Cafe cf2) {
            return (int) (cf1.getCafeOfAllTimeRank() - cf2.getCafeOfAllTimeRank());
        }
    }
}
