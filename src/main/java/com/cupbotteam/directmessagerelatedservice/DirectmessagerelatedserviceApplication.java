package com.cupbotteam.directmessagerelatedservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
@EnableAsync
public class DirectmessagerelatedserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DirectmessagerelatedserviceApplication.class, args);
    }

    @RequestMapping(value = "/")
    public String home() {
        return "Eureka Client application";
    }
}
