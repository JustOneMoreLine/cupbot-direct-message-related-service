package com.cupbotteam.directmessagerelatedservice.core;

/**
 * A java class use to set a JSON structure for sending HTTP request,
 * especially between direct message related service and notification checker.
 * This would hold a direct message's sender and receiver twitter ids, a direct
 * message's id, and its content.
 */
public class DirectMessageJson {
    private long dmId;
    private long senderID;
    private long receiverID;
    private String text;

    public void setDmId(long dmID) {
        this.dmId = dmID;
    }

    public Long getDmId() {
        return dmId;
    }

    public void setSenderID(long senderID) {
        this.senderID = senderID;
    }

    public Long getSenderID() {
        return senderID;
    }

    public void setReceiverID(long receiverID) {
        this.receiverID = receiverID;
    }

    public Long getReceiverID() {
        return receiverID;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "DirectMessageJson{" +
                "DmID=" + dmId +
                ", senderID=" + senderID +
                ", receiverID=" + receiverID +
                ", text='" + text + '\'' +
                '}';
    }
}
