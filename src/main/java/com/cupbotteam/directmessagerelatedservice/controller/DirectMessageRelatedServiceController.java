package com.cupbotteam.directmessagerelatedservice.controller;

import com.cupbotteam.directmessagerelatedservice.core.CrcToken;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.Authentication;
import com.cupbotteam.directmessagerelatedservice.service.keyword.KeywordHandlerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.concurrent.Callable;

@Controller
public class DirectMessageRelatedServiceController {
    private Authentication authentication;
    private KeywordHandlerController keywordHandlerController;

    @Autowired
    public DirectMessageRelatedServiceController(
            Authentication authentication,
            KeywordHandlerController keywordHandlerController) {
        this.authentication = authentication;
        this.keywordHandlerController = keywordHandlerController;
    }

    /**
     * An endpoint to ping the service and keep it alive,
     * may also be use to trigger a function that required to run
     * daily.
     *
     * @return ResponseEntity with 200 HTTP Status
     */
    @GetMapping("/wakeMe")
    public ResponseEntity wakeMe() {
        authentication.checkOutdateTokens();
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * An endpoint to give other service a challange token,
     * which would be use for authentication.
     *
     * @return CrcToken, a JSON that contains one value named "token"
     */
    @GetMapping("/challange")
    @ResponseBody
    public ResponseEntity<CrcToken> challange() {
        CrcToken token = new CrcToken();
        token.setToken(authentication.generateCrcToken());
        return ResponseEntity.ok(token);
    }

    /**
     * An endpoint for Notification Checker Service to give
     * new direct messages.
     *
     * @param response , a token made from previous challange token, for authentication.
     * @param message , the new direct message sent by Notification Checker Service.
     * @return ResponseEntity, HTTP STATUS OK if authentication has passed,
     * and HTTP STATUS FORBIDDEN if authentication fails.
     */
    @PostMapping("/webhook")
    @ResponseBody
    public ResponseEntity directMessageWebhook(@RequestParam String response,
                                               @RequestBody DirectMessageJson message) {
        System.out.println(message.getText());
        System.out.println(response);
        if (authentication.authenticateResponseToken(response)) {
            keywordHandlerController.handle(message);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    /**
     * An endpoint for Notification Checker Service to give
     * new direct messages asynchronously.
     *
     * @param response , a token made from previous challange token, for authentication.
     * @param message , the new direct message sent by Notification Checker Service.
     * @return ResponseEntity, HTTP STATUS OK if authentication has passed,
     * and HTTP STATUS FORBIDDEN if authentication fails.
     */
    @Async
    @PostMapping("/asyncWebhook")
    @ResponseBody
    public Callable<ResponseEntity> asyncDirectMessageWebhook(@RequestParam String response,
                                                                       @RequestBody DirectMessageJson message) {
        if (authentication.authenticateResponseToken(response)) {
            keywordHandlerController.handle(message);
            return () -> new ResponseEntity(HttpStatus.OK);
        } else {
            return () -> new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }
} // end
