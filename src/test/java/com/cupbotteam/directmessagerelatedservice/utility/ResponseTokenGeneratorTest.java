package com.cupbotteam.directmessagerelatedservice.utility;

import com.cupbotteam.directmessagerelatedservice.core.CrcToken;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ResponseTokenGeneratorTest {
    @Mock
    RestTemplate ask;

    public CrcToken dummyToken() {
        CrcToken crcToken = new CrcToken();
        crcToken.setToken("1234567890");
        return crcToken;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testResponse() {
        ResponseTokenGenerator.setAsk(ask);
        doReturn(dummyToken()).when(ask).getForObject("https://hostName/challange", CrcToken.class);
        ResponseTokenGenerator.generate("hostName");
        verify(ask).getForObject("https://hostName/challange", CrcToken.class);
        ResponseTokenGenerator.setAsk(null);
    }

    @Test
    public void testResponseWithNotMock() {
        doReturn(dummyToken()).when(ask).getForObject("https://hostName/challange", CrcToken.class);
        try {
            ResponseTokenGenerator.generate("hostName");
        } catch (Exception e) {
            assertNotEquals(ResponseTokenGenerator.ask, new RestTemplate());
        }
        ResponseTokenGenerator.setAsk(null);
    }
}
