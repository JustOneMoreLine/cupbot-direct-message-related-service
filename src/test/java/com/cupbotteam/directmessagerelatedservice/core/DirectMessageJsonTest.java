package com.cupbotteam.directmessagerelatedservice.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DirectMessageJsonTest {

    @Test
    public void directMessageJsonTest() {
        DirectMessageJson test = new DirectMessageJson();
        assertEquals(test.getDmId(), 0);
        assertEquals(test.getSenderID(), 0);
        assertEquals(test.getReceiverID(), 0);
        assertNull(test.getText());

        test.setDmId(123);
        assertEquals(test.getDmId(), 123);

        test.setSenderID(123);
        assertEquals(test.getSenderID(), 123);

        test.setReceiverID(123);
        assertEquals(test.getReceiverID(), 123);

        test.setText("ABC");
        assertEquals(test.getText(), "ABC");

        assertEquals(test.toString(),
                "DirectMessageJson{DmID=123, senderID=123, receiverID=123, text='ABC'}");
    }
}
