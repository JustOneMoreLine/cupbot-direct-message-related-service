package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EndHandlerTest {
    EndHandler endHandler;

    @BeforeEach
    public void setUp() {
        endHandler = new EndHandler();
    }

    @Test
    public void testEndHandler() {
        boolean result = endHandler.handle(new DirectMessageJson());
        assertTrue(result);

        endHandler.setNextHandler(new EndHandler());
        assertNull(endHandler.endHandler);
    }
}
