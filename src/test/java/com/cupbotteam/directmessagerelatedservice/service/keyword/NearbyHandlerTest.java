package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeNearby;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class NearbyHandlerTest {
    NearbyHandler nearbyHandler;
    @Mock
    CafeNearby nearbyImplMock;
    CafeAddress cafeAddress;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        nearbyHandler = new NearbyHandler(nearbyImplMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!nearby Cinere/Depok/Jawa Barat");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!wrongkeyword Cinere/Depok/Jawa Barat");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!nearby cinere depok");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeyword();
        nearbyHandler.handleRequest(message);
        verify(nearbyImplMock).showNearby(message.getSenderID(), "Cinere", "Depok", "Jawa Barat");
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        nearbyHandler.handleRequest(message);
        verify(nearbyImplMock, never()).showNearby(message.getSenderID(), "Cinere", "Depok", "Jawa Barat");
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        nearbyHandler.handleRequest(message);
        verify(nearbyImplMock, never()).showNearby(message.getSenderID(), "Cinere", "Depok", "Jawa Barat");
    }
}
