package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeRegistration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class KeyHandCafeShowMyRegisterCafeTest {
    KeyHandShowMyRegisterCafe keyHandShowMyRegisterCafe;
    @Mock
    CafeRegistration cafeRegistrationImplMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        keyHandShowMyRegisterCafe = new KeyHandShowMyRegisterCafe(cafeRegistrationImplMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordCorrectFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!showMyCafe");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeywordCorrectFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!wubawuba");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!showMyCafe Starbucks");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!wubbawubba lol");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeywordCorrectFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordCorrectFormat();
        keyHandShowMyRegisterCafe.handleRequest(message);
        verify(cafeRegistrationImplMock).showMyCafe(any(Long.class));
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        keyHandShowMyRegisterCafe.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).showMyCafe(any(Long.class));
    }

    @Test
    public void incomingDmWithWrongKeywordCorrectFormat() {
        DirectMessageJson message = incomingDmExampleWithWrongKeywordCorrectFormat();
        keyHandShowMyRegisterCafe.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).showMyCafe(any(Long.class));
    }

    @Test
    public void incomingDmWithWrongKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithWrongKeywordWrongFormat();
        keyHandShowMyRegisterCafe.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).showMyCafe(any(Long.class));
    }
}
