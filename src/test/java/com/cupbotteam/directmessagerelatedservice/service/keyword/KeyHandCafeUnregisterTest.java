package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeRegistration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class KeyHandCafeUnregisterTest {
    KeyHandCafeUnregister keyHandCafeUnregister;
    @Mock
    CafeRegistration cafeRegistration;
    CafeId cafeId;
    CafeAddress cafeAddress;

    public DirectMessageJson incomingDmExampleWithCorrectKeywordCorrectFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeUnregister Starbucks");
        cafeId = new CafeId();
        cafeId.setCafeOwnerTwitterId(67890);
        cafeId.setCafeName("Starbucks");
        cafeAddress = new CafeAddress();
        cafeAddress.setStreet("Jl Merak 3 No 17 KDA");
        cafeAddress.setSubDistrict("Belian");
        cafeAddress.setDistrict("Batam Kota");
        cafeAddress.setCity("Batam");
        cafeAddress.setProvince("Kepulauan Riau");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!lubadubdub Starbucks/Jl Merak 3 No 17 KDA/Belian/Batam Kota/Batam/Kepulauan Riau");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeUnregister starbucks bogor/Bogor");
        return result;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        keyHandCafeUnregister = new KeyHandCafeUnregister(cafeRegistration);
    }

    @AfterEach
    public void cleanUp() {
        cafeId = null;
        cafeAddress = null;
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        keyHandCafeUnregister.handleRequest(message);
        verify(cafeRegistration, never()).unregisterCafe(any(CafeId.class));
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        keyHandCafeUnregister.handleRequest(message);
        verify(cafeRegistration, never()).unregisterCafe(any(CafeId.class));
    }

    @Test
    public void incomingDmWithCorrectKeywordCorrectFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordCorrectFormat();
        keyHandCafeUnregister.handleRequest(message);
        verify(cafeRegistration).unregisterCafe(any(CafeId.class));
    }
}
