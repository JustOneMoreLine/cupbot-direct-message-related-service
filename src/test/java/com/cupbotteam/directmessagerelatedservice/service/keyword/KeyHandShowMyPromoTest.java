package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafePromoCode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class KeyHandShowMyPromoTest {
    KeyHandShowMyPromo keyHandShowMyPromo;
    @Mock
    CafePromoCode cafePromoCodeImplMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        keyHandShowMyPromo = new KeyHandShowMyPromo(cafePromoCodeImplMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordCorrectFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!showMyPromo");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeywordCorrectFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!bukanShowMyPromo");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!showMyPromo Cafe TD!");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!bukanShowMyPromo lol");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeywordCorrectFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordCorrectFormat();
        keyHandShowMyPromo.handleRequest(message);
        verify(cafePromoCodeImplMock).showMyPromo(any(Long.class));
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        keyHandShowMyPromo.handleRequest(message);
        verify(cafePromoCodeImplMock, never()).showMyPromo(any(Long.class));
    }

    @Test
    public void incomingDmWithWrongKeywordCorrectFormat() {
        DirectMessageJson message = incomingDmExampleWithWrongKeywordCorrectFormat();
        keyHandShowMyPromo.handleRequest(message);
        verify(cafePromoCodeImplMock, never()).showMyPromo(any(Long.class));
    }

    @Test
    public void incomingDmWithWrongKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithWrongKeywordWrongFormat();
        keyHandShowMyPromo.handleRequest(message);
        verify(cafePromoCodeImplMock, never()).showMyPromo(any(Long.class));
    }
}
