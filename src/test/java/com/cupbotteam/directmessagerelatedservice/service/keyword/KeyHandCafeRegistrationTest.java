package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeRegistration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class KeyHandCafeRegistrationTest {
    KeyHandCafeRegistration keyHandCafeRegistration;
    @Mock
    CafeRegistration cafeRegistrationImplMock;
    CafeId cafeId;
    CafeAddress cafeAddress;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        keyHandCafeRegistration = new KeyHandCafeRegistration(cafeRegistrationImplMock);
    }

    @AfterEach
    public void cleanUp() {
        cafeId = null;
        cafeAddress = null;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeRegister Starbucks/Jl Merak 3 No 17 KDA/Belian/Batam Kota/Batam/Kepulauan Riau");
        cafeId = new CafeId();
        cafeId.setCafeOwnerTwitterId(67890);
        cafeId.setCafeName("Starbucks");
        cafeAddress = new CafeAddress();
        cafeAddress.setStreet("Jl Merak 3 No 17 KDA");
        cafeAddress.setSubDistrict("Belian");
        cafeAddress.setDistrict("Batam Kota");
        cafeAddress.setCity("Batam");
        cafeAddress.setProvince("Kepulauan Riau");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!lubadubdub Starbucks/Jl Merak 3 No 17 KDA/Belian/Batam Kota/Batam/Kepulauan Riau");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!cafeRegister starbucks bogor/Bogor");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeyword();
        keyHandCafeRegistration.handleRequest(message);
        verify(cafeRegistrationImplMock).registerCafe(any(CafeId.class), any(CafeAddress.class));
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        keyHandCafeRegistration.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).registerCafe(cafeId, cafeAddress);
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        keyHandCafeRegistration.handleRequest(message);
        verify(cafeRegistrationImplMock, never()).registerCafe(cafeId, cafeAddress);
    }
}
