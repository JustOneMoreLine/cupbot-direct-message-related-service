package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class BaseKeywordHandlerTest {
    @InjectMocks
    MockBaseExtensionTrue test;
    @InjectMocks
    MockBaseExtensionFalse nextHandler1;
    @InjectMocks
    MockBaseExtensionFalse nextHandler2;
    @Mock
    BaseKeywordHandler spyNextHandler1;
    @Mock
    EndHandler endHandler;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        test = new MockBaseExtensionTrue(endHandler);
        nextHandler1 = new MockBaseExtensionFalse(endHandler);
        nextHandler2 = new MockBaseExtensionFalse(endHandler);
    }

    @Test
    public void testBaseHandler() {
        test.handleRequest(new DirectMessageJson());
        verify(endHandler, never()).handleRequest(any(DirectMessageJson.class));

        nextHandler1.setNextHandler(spyNextHandler1);
        nextHandler1.handleRequest(new DirectMessageJson());
        verify(spyNextHandler1).handleRequest(any(DirectMessageJson.class));

        nextHandler2.handleRequest(new DirectMessageJson());
        verify(endHandler).handleRequest(any(DirectMessageJson.class));
    }
}
class MockBaseExtensionTrue extends BaseKeywordHandler {

    public MockBaseExtensionTrue(EndHandler endHandler) {
        this.endHandler = endHandler;
    }
    @Override
    public boolean handle(DirectMessageJson message) {
        return true;
    }
}

class MockBaseExtensionFalse extends BaseKeywordHandler {

    public MockBaseExtensionFalse(EndHandler endHandler) {
        this.endHandler = endHandler;
    }
    @Override
    public boolean handle(DirectMessageJson message) {
        return false;
    }
}
