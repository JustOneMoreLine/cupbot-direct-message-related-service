package com.cupbotteam.directmessagerelatedservice.service.keyword;


import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.CafePromo;
import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafePromoCode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class KeyHandRegisPromoTest {
    KeyHandRegisPromo keyHandRegisPromo;
    @Mock
    CafePromoCode cafePromoCodeImplMock;
    CafeId cafeId;
    CafePromo cafePromo;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        keyHandRegisPromo = new KeyHandRegisPromo(cafePromoCodeImplMock);
    }

    @AfterEach
    public void cleanUp() {
        cafeId = null;
        cafePromo = null;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!regisPromo Cafe TSA/ISEEDEADBODY/2020-06-02/2020-08-03/Ini bukan promo cafe TD!");
        cafePromo = new CafePromo();
        System.out.println(result.getSenderID());
        cafePromo.setId(result.getSenderID());
        cafeId = new CafeId();
        cafeId.setCafeOwnerTwitterId(67890);
        cafeId.setCafeName("Cafe TSA");
        cafePromo.setPromoCode("ISEEDEADBODY");
        String date = "2020-06-02 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        cafePromo.setStartOfPromo(dateTime);
        String date2 = "2020-08-03 00:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(date2, formatter);
        cafePromo.setEndOfPromo(dateTime2);
        cafePromo.setPromoDescription("Ini bukan promo cafe TD!");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!bukanRegisPromo Cafe TSA/ISEEDEADBODY/2020-06-02/2020-08-03/Ini bukan promo cafe TD!");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!regisPromo Cafe TSA/ISEEDEADBODY/Ini bukan promo cafe TD!");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeyword();
        keyHandRegisPromo.handleRequest(message);
        String date = "2020-06-02 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        String date2 = "2020-08-03 00:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(date2, formatter);
        verify(cafePromoCodeImplMock).regisPromo(eq(message.getSenderID()), any(CafeId.class),
                eq("ISEEDEADBODY"), eq(dateTime),
                eq(dateTime2), eq("Ini bukan promo cafe TD!"));
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        keyHandRegisPromo.handleRequest(message);
        String date = "2020-06-02 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        String date2 = "2020-08-03 00:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(date2, formatter);
        verify(cafePromoCodeImplMock, never()).regisPromo(eq(1), any(CafeId.class),
                eq("ISEEDEADBODY"), eq(dateTime),
                eq(dateTime2), eq("Ini bukan promo cafe TD!"));
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        keyHandRegisPromo.handleRequest(message);
        String date = "2020-06-02 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        String date2 = "2020-08-03 00:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(date2, formatter);
        verify(cafePromoCodeImplMock, never()).regisPromo(eq(1), any(CafeId.class),
                eq("ISEEDEADBODY"), eq(dateTime),
                eq(dateTime2), eq("Ini bukan promo cafe TD!"));
    }
}
