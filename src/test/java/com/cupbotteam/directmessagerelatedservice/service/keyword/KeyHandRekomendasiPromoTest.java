package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafePromoCode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class KeyHandRekomendasiPromoTest {
    KeyHandRekomendasiPromo keyHandRekomendasiPromo;
    @Mock
    CafePromoCode cafePromoCodeImplMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        keyHandRekomendasiPromo = new KeyHandRekomendasiPromo(cafePromoCodeImplMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordCorrectFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!rekomendasiPromo");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeywordCorrectFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!bukanRekomendasiPromo");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!rekomendasiPromo Cafe TD!");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeywordWrongFormat() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!bukanRekomendasiPromo lol");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeywordCorrectFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordCorrectFormat();
        keyHandRekomendasiPromo.handleRequest(message);
        verify(cafePromoCodeImplMock).rekomendasiPromo(any(Long.class));
    }

    @Test
    public void incomingDmWithCorrectKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeywordWrongFormat();
        keyHandRekomendasiPromo.handleRequest(message);
        verify(cafePromoCodeImplMock, never()).rekomendasiPromo(any(Long.class));
    }

    @Test
    public void incomingDmWithWrongKeywordCorrectFormat() {
        DirectMessageJson message = incomingDmExampleWithWrongKeywordCorrectFormat();
        keyHandRekomendasiPromo.handleRequest(message);
        verify(cafePromoCodeImplMock, never()).rekomendasiPromo(any(Long.class));
    }

    @Test
    public void incomingDmWithWrongKeywordWrongFormat() {
        DirectMessageJson message = incomingDmExampleWithWrongKeywordWrongFormat();
        keyHandRekomendasiPromo.handleRequest(message);
        verify(cafePromoCodeImplMock, never()).rekomendasiPromo(any(Long.class));
    }
}
