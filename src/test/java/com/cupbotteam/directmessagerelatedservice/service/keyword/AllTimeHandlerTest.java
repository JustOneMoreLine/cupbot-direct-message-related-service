package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.apps.CafeAllTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class AllTimeHandlerTest {
    AllTimeHandler allTimeHandler;
    @Mock
    CafeAllTime allTimeImplMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        allTimeHandler = new AllTimeHandler(allTimeImplMock);
    }

    public DirectMessageJson incomingDmExampleWithCorrectKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!allTime ");
        return result;
    }

    public DirectMessageJson incomingDmExampleWithWrongKeyword() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(12345);
        result.setSenderID(67890);
        result.setReceiverID(151515);
        result.setText("!wrongkeyword ");
        return result;
    }

    @Test
    public void incomingDmWithCorrectKeyword() {
        DirectMessageJson message = incomingDmExampleWithCorrectKeyword();
        allTimeHandler.handleRequest(message);
        verify(allTimeImplMock).showAllTime(message.getSenderID());
    }

    @Test
    public void incomingDmWithWrongKeyword() {
        DirectMessageJson message = incomingDmExampleWithWrongKeyword();
        allTimeHandler.handleRequest(message);
        verify(allTimeImplMock, never()).showAllTime(message.getSenderID());
    }
}
