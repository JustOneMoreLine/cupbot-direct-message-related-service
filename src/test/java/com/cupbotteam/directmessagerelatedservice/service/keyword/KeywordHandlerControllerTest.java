package com.cupbotteam.directmessagerelatedservice.service.keyword;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KeywordHandlerControllerTest {
    @InjectMocks
    KeywordHandlerControllerImpl keywordHandlerController;
    @Mock
    MockHandleFalse failedHandle;
    @Mock
    MockHandleTrue successHandle;
    @Mock
    EndHandler endHandler;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        failedHandle = new MockHandleFalse(endHandler);
        successHandle = new MockHandleTrue(endHandler);
    }

    @Test
    public void testKeywordHandler() {
        List<KeywordHandler> list1 = new ArrayList<>();
        List<KeywordHandler> list2 = new ArrayList<>();
        list1.add(failedHandle);
        list2.add(failedHandle);
        list2.add(successHandle);

        keywordHandlerController = new KeywordHandlerControllerImpl(list1);
        assertTrue(keywordHandlerController.isItNotSetUp);

        keywordHandlerController.handle(new DirectMessageJson());
        assertFalse(keywordHandlerController.isItNotSetUp);
        assertEquals(keywordHandlerController.firstHandler, failedHandle);

        keywordHandlerController = new KeywordHandlerControllerImpl(list2);
        assertTrue(keywordHandlerController.isItNotSetUp);

        keywordHandlerController.handle(new DirectMessageJson());
        assertFalse(keywordHandlerController.isItNotSetUp);
        assertEquals(keywordHandlerController.firstHandler, failedHandle);
    }
}

class MockHandleTrue extends BaseKeywordHandler {

    public MockHandleTrue(EndHandler endHandler) {
        this.endHandler = endHandler;
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        return true;
    }
}

class MockHandleFalse extends BaseKeywordHandler {

    public MockHandleFalse(EndHandler endHandler) {
        this.endHandler = endHandler;
    }

    @Override
    public boolean handle(DirectMessageJson message) {
        return false;
    }
}
