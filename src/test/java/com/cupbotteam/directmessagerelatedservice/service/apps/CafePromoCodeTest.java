package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.core.CafePromo;
import com.cupbotteam.directmessagerelatedservice.service.PromoDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class CafePromoCodeTest {
    @Mock
    Twitter4JAdapter twitter4JAdapterImplMock;
    @Spy
    PromoDatabaseAdapter promoDatabaseAdapter;
    @InjectMocks
    CafePromoCodeImpl serviceTested;
    @Captor
    ArgumentCaptor argumentCaptor;

    public CafeId newCafeId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("cetarBak");
        return result;
    }

    public CafePromo newCafePromo(CafeId cafeId) {
        CafePromo result = new CafePromo();
        String date = "2020-06-02 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        String date2 = "2020-08-03 00:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(date2, formatter);
        result.setId(1);
        result.setCafeId(cafeId);
        result.setStartOfPromo(dateTime);
        result.setEndOfPromo(dateTime2);
        result.setPromoCode("ISEEDEADBODY");
        result.setPromoDescription("Ini bukan promo cafe TD!");
        return result;
    }

    public CafeId newCafeId2() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(15);
        result.setCafeName("MOONSHINE");
        return result;
    }

    public CafePromo newCafePromo2(CafeId cafeId) {
        CafePromo result = new CafePromo();
        String date = "2020-06-02 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        String date2 = "2020-08-03 00:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(date2, formatter);
        result.setId(2);
        result.setCafeId(cafeId);
        result.setStartOfPromo(dateTime);
        result.setEndOfPromo(dateTime2);
        result.setPromoCode("AEZAKMI");
        result.setPromoDescription("Nikmati Promo 100% hanya di Cafe TD!");
        return result;
    }


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        serviceTested = new CafePromoCodeImpl(twitter4JAdapterImplMock, promoDatabaseAdapter);
    }

    @Test
    public void testRegisterNewPromoSuccess() {
        // Setup: cafe registration success
        CafeId cafeId = newCafeId();
        CafePromo cafePromo = newCafePromo(cafeId);
        doReturn(new CafePromo[0]).when(promoDatabaseAdapter).getPromoById(cafePromo.getId());
        doReturn(cafePromo).when(promoDatabaseAdapter).addPromo(any(CafePromo.class));
        doReturn(new CafePromo[0]).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.regisPromo(cafePromo.getId(), cafePromo.getCafeId(), cafePromo.getPromoCode(),
                cafePromo.getStartOfPromo(), cafePromo.getEndOfPromo(), cafePromo.getPromoDescription());
        verify(promoDatabaseAdapter).getPromoById(cafePromo.getId());
        verify(promoDatabaseAdapter).addPromo(any(CafePromo.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                cafePromo.getId() + " have been successfully added! Welcome to the club!");
    }

    @Test
    public void testRegisterKnownPromoFail() {
        // Setup: cafe registration fails because the same cafe have been registered
        CafeId cafeId = newCafeId();
        CafePromo cafePromo = newCafePromo(cafeId);
        CafePromo[] promoOnDatabase = new CafePromo[1];
        promoOnDatabase[0] = cafePromo;
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getPromoById(cafePromo.getId());
        doReturn(cafePromo).when(promoDatabaseAdapter).addPromo(any(CafePromo.class));
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.regisPromo(cafePromo.getId(), cafePromo.getCafeId(), cafePromo.getPromoCode(),
                cafePromo.getStartOfPromo(), cafePromo.getEndOfPromo(), cafePromo.getPromoDescription());
        verify(promoDatabaseAdapter).getPromoById(cafePromo.getId());
        verify(promoDatabaseAdapter, never()).addPromo(any(CafePromo.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "I'am sorry, but \"" + cafeId.getCafeName() + "\" has already been registered");
    }

    /*
    @Test
    public void testRegisterNewCafeFail2() {
        // Setup: cafe registration fails because database fail to save new cafe
        CafeId cafeId = newCafeId();
        String date = "2020-06-02 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        String date2 = "2020-08-03 00:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(date2, formatter);
        CafePromo[] promoOnDatabase = new CafePromo[0];
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getPromoById(1);
        doReturn(null).when(promoDatabaseAdapter).addPromo(any(CafePromo.class));
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.regisPromo(1, cafeId, "AEZAKMI", dateTime, dateTime2,
                "Ini Deskripsinya!");
        verify(promoDatabaseAdapter).getPromoById(1);
        //verify(promoDatabaseAdapter).getAllPromo();
        verify(promoDatabaseAdapter).addPromo(any(CafePromo.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something went wrong in our end, please try again later.");
    }

    @Test
    public void testRegisterNewCafeFail3() {
        // Setup: cafe registration fails because database fail to save the correct cafe
        CafeId cafeId = newCafeId();
        CafePromo[] promoOnDatabase = new CafePromo[0];
        CafePromo cafePromo = newCafePromo(cafeId);
        CafeId cafeId2 = newCafeId2();
        CafePromo incorrectCafePromo = newCafePromo2(cafeId2);
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getPromoById(cafePromo.getId());
        doReturn(incorrectCafePromo).when(promoDatabaseAdapter).addPromo(any(CafePromo.class));
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.regisPromo(cafePromo.getId(), cafePromo.getCafeId(), cafePromo.getPromoCode(),
                cafePromo.getStartOfPromo(), cafePromo.getEndOfPromo(), cafePromo.getPromoDescription());
        verify(promoDatabaseAdapter).getPromoById(cafePromo.getId());
        //verify(promoDatabaseAdapter).getAllPromo();
        verify(promoDatabaseAdapter).addPromo(any(CafePromo.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something went wrong in our end, please try again later.");
    }


    @Test
    public void testCafeUnregisterButCafeDoesntExistFail() {
        // Setup: cafe unregister fails because requested cafe doesn't exist
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[0];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        Cafe[] deletedCafe = new Cafe[1];
        deletedCafe[0] = cafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter, never()).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                cafeId.getCafeName() + " is not registered.");
    }

    @Test
    public void testCafeUnregisterSuccess() {
        // Setup: cafe unregister success
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[1];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        cafeOnDatabase[0] = cafe;
        Cafe[] deletedCafe = new Cafe[1];
        deletedCafe[0] = cafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                cafeId.getCafeName() + " have been unregistered. It's sad to see you go :(");
    }

    @Test
    public void testCafeUnregisterFail() {
        // Setup: cafe unregister fails because database fails to delete
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[1];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        cafeOnDatabase[0] = cafe;
        Cafe[] deletedCafe = new Cafe[0];
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something wrong happen in our end, please try again later.");
    }

    @Test
    public void testCafeUnregisterFail2() {
        // Setup: cafe unregister fails because database deleted the wrong cafe
        CafeId cafeId = newCafeId();
        CafeId cafeId2 = newCafeId2();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[1];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        cafeOnDatabase[0] = cafe;
        Cafe[] deletedCafe = new Cafe[1];
        Cafe incorrectCafe = new Cafe(cafeId2, cafeAddress);
        deletedCafe[0] = incorrectCafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something wrong happen in our end, please try again later.");
    }

    */

    @Test
    public void testShowMyPromoSuccess() {
        // Setup: show all registered cafe success
        CafeId cafeId = newCafeId();
        CafePromo[] promoOnDatabase = new CafePromo[1];
        CafePromo cafePromo = newCafePromo(cafeId);
        //cafePromo.setCafeOfAllTimeRank(1);
        promoOnDatabase[0] = cafePromo;
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.showMyPromo(cafeId.getCafeOwnerTwitterId());
        verify(promoDatabaseAdapter).getAllPromo();
                //getCafePromo(cafePromo.getCafeId());
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Promo(s):\n" +
                        "\n" +
                        cafePromo.getCafeId().getCafeName() + "\n"
                        + "ID Promo : " + cafePromo.getId() + "\n"
                        + "Cafe Promo Code : " + cafePromo.getPromoCode() + "\n"
                        + "Deskripsi: " + cafePromo.getPromoDescription() + "\n");
    }

    @Test
    public void testShowMyPromoNoRegisteredCafeSuccess() {
        // Setup: show all registered cafe success but no cafe are registered
        CafeId cafeId = newCafeId();
        CafePromo[] promoOnDatabase = new CafePromo[0];
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.showMyPromo(cafeId.getCafeOwnerTwitterId());
        verify(promoDatabaseAdapter).getAllPromo();
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "No Promo is registered under this twitter account.");
    }

    @Test
    public void testRekomendasiPromoSuccess() {
        // Setup: show all registered cafe success
        CafeId cafeId = newCafeId();
        CafePromo[] promoOnDatabase = new CafePromo[1];
        CafePromo cafePromo = newCafePromo(cafeId);
        //cafePromo.setCafeOfAllTimeRank(1);
        promoOnDatabase[0] = cafePromo;
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.rekomendasiPromo(cafeId.getCafeOwnerTwitterId());
        verify(promoDatabaseAdapter).getAllPromo();
        //getCafePromo(cafePromo.getCafeId());
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Promo(s):\n" +
                        "\n" +
                        cafePromo.getCafeId().getCafeName() + "\n"
                        + "ID Promo : " + cafePromo.getId() + "\n"
                        + "Cafe Promo Code : " + cafePromo.getPromoCode() + "\n"
                        + "Deskripsi: " + cafePromo.getPromoDescription() + "\n");
    }

    @Test
    public void testRekomendasiPromoNoRegisteredCafeSuccess() {
        // Setup: show all registered cafe success but no cafe are registered
        CafeId cafeId = newCafeId();
        CafePromo[] promoOnDatabase = new CafePromo[0];
        doReturn(promoOnDatabase).when(promoDatabaseAdapter).getAllPromo();

        serviceTested.rekomendasiPromo(cafeId.getCafeOwnerTwitterId());
        verify(promoDatabaseAdapter).getAllPromo();
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Maaf saat ini sedang tidak ada promo yang berlaku.");
    }

}
