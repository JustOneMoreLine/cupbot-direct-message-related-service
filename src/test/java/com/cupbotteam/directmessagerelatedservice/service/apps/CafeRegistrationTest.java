package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.service.CafeDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class CafeRegistrationTest {
    @Mock
    Twitter4JAdapter twitter4JAdapterImplMock;
    @Spy
    CafeDatabaseAdapter cafeDatabaseAdapter;
    @InjectMocks
    CafeRegistrationImpl serviceTested;
    @Captor
    ArgumentCaptor argumentCaptor;

    public CafeId newCafeId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("cetarBak");
        return result;
    }

    public CafeId newCafeId2() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(15);
        result.setCafeName("MOONSHINE");
        return result;
    }

    public CafeAddress newAddress() {
        CafeAddress result = new CafeAddress();
        result.setStreet("A");
        result.setSubDistrict("B");
        result.setDistrict("C");
        result.setCity("D");
        result.setProvince("E");
        return result;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        serviceTested = new CafeRegistrationImpl(twitter4JAdapterImplMock, cafeDatabaseAdapter);
    }

    @Test
    public void testRegisterNewCafeSuccess() {
        // Setup: cafe registration success
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        doReturn(new Cafe[0]).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(cafe).when(cafeDatabaseAdapter).addCafe(any(Cafe.class));
        doReturn(new Cafe[0]).when(cafeDatabaseAdapter).getAllCafe();

        serviceTested.registerCafe(cafeId, cafeAddress);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).addCafe(any(Cafe.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                cafeId.getCafeName() + " have been successfully added! Welcome to the club!");
    }

    @Test
    public void testRegisterKnownCafeFail() {
        // Setup: cafe registration fails because the same cafe have been registered
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        Cafe[] cafeOnDatabase = new Cafe[1];
        cafeOnDatabase[0] = cafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(cafe).when(cafeDatabaseAdapter).addCafe(any(Cafe.class));
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();

        serviceTested.registerCafe(cafeId, cafeAddress);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter, never()).addCafe(any(Cafe.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "I'am sorry, but \"" + cafeId.getCafeName() + "\" has already been registered");
    }

    @Test
    public void testRegisterNewCafeFail2() {
        // Setup: cafe registration fails because database fail to save new cafe
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[0];
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(null).when(cafeDatabaseAdapter).addCafe(any(Cafe.class));
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();

        serviceTested.registerCafe(cafeId, cafeAddress);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).getAllCafe();
        verify(cafeDatabaseAdapter).addCafe(any(Cafe.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something went wrong in our end, please try again later.");
    }

    @Test
    public void testRegisterNewCafeFail3() {
        // Setup: cafe registration fails because database fail to save the correct cafe
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[0];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        CafeId cafeId2 = newCafeId2();
        Cafe incorrectCafe = new Cafe(cafeId2, cafeAddress);
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(incorrectCafe).when(cafeDatabaseAdapter).addCafe(any(Cafe.class));
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();

        serviceTested.registerCafe(cafeId, cafeAddress);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).getAllCafe();
        verify(cafeDatabaseAdapter).addCafe(any(Cafe.class));
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something went wrong in our end, please try again later.");
    }

    @Test
    public void testCafeUnregisterButCafeDoesntExistFail() {
        // Setup: cafe unregister fails because requested cafe doesn't exist
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[0];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        Cafe[] deletedCafe = new Cafe[1];
        deletedCafe[0] = cafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter, never()).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                cafeId.getCafeName() + " is not registered.");
    }

    @Test
    public void testCafeUnregisterSuccess() {
        // Setup: cafe unregister success
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[1];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        cafeOnDatabase[0] = cafe;
        Cafe[] deletedCafe = new Cafe[1];
        deletedCafe[0] = cafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                cafeId.getCafeName() + " have been unregistered. It's sad to see you go :(");
    }

    @Test
    public void testCafeUnregisterFail() {
        // Setup: cafe unregister fails because database fails to delete
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[1];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        cafeOnDatabase[0] = cafe;
        Cafe[] deletedCafe = new Cafe[0];
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something wrong happen in our end, please try again later.");
    }

    @Test
    public void testCafeUnregisterFail2() {
        // Setup: cafe unregister fails because database deleted the wrong cafe
        CafeId cafeId = newCafeId();
        CafeId cafeId2 = newCafeId2();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[1];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        cafeOnDatabase[0] = cafe;
        Cafe[] deletedCafe = new Cafe[1];
        Cafe incorrectCafe = new Cafe(cafeId2, cafeAddress);
        deletedCafe[0] = incorrectCafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafe(cafeId);
        doReturn(deletedCafe).when(cafeDatabaseAdapter).deleteCafe(cafeId);

        serviceTested.unregisterCafe(cafeId);
        verify(cafeDatabaseAdapter).getCafe(cafeId);
        verify(cafeDatabaseAdapter).deleteCafe(cafeId);
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry! Something wrong happen in our end, please try again later.");
    }

    @Test
    public void testShowMyCafeSuccess() {
        // Setup: show all registered cafe success
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe[] cafeOnDatabase = new Cafe[1];
        Cafe cafe = new Cafe(cafeId, cafeAddress);
        cafe.setCafeOfAllTimeRank(1);
        cafeOnDatabase[0] = cafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafeByOwnerTwitterId(cafeId.getCafeOwnerTwitterId());

        serviceTested.showMyCafe(cafeId.getCafeOwnerTwitterId());
        verify(cafeDatabaseAdapter).getCafeByOwnerTwitterId(cafeId.getCafeOwnerTwitterId());
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "My Cafe(s):\n" +
                        "\n" +
                        "cetarBak\n" +
                        "Cafe Address: " + cafeAddress.toString() + "\n" +
                        "Cafe Rank: 1\n" +
                        "Cafe of The Week Wins: 0");
    }

    @Test
    public void testShowMyCafeNoRegisteredCafeSuccess() {
        // Setup: show all registered cafe success but no cafe are registered
        CafeId cafeId = newCafeId();
        Cafe[] cafeOnDatabase = new Cafe[0];
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getCafeByOwnerTwitterId(cafeId.getCafeOwnerTwitterId());

        serviceTested.showMyCafe(cafeId.getCafeOwnerTwitterId());
        verify(cafeDatabaseAdapter).getCafeByOwnerTwitterId(cafeId.getCafeOwnerTwitterId());
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "No cafe is registered under this twitter account.");
    }
}
