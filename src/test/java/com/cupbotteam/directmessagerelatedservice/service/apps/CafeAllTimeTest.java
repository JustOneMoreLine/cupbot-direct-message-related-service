package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.service.CafeDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class CafeAllTimeTest {
    @Mock
    Twitter4JAdapter twitter4JAdapterImplMock;
    @Spy
    CafeDatabaseAdapter cafeDatabaseAdapter;
    @InjectMocks
    CafeAllTimeImpl serviceTested;
    @Captor
    ArgumentCaptor argumentCaptor;

    public CafeId newCafeIdF1() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Blue Bottle");
        return result;
    }

    public CafeId newCafeIdF2() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Fore");
        return result;
    }

    public CafeId newCafeIdF3() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Starbucks");
        return result;
    }

    public CafeId newCafeIdF4() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Maxx");
        return result;
    }

    public CafeId newCafeIdF5() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Kitchenette");
        return result;
    }

    public CafeId newCafeIdF6() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Koi");
        return result;
    }

    public CafeId newCafeIdF7() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Warkop 96");
        return result;
    }

    public CafeId newCafeIdF8() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Warkop 97");
        return result;
    }

    public CafeId newCafeIdF9() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Warkop 98");
        return result;
    }

    public CafeId newCafeIdF10() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Warkop 99");
        return result;
    }
    
    public CafeId newCafeNone() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Nonexistent");
        return result;
    }

    public CafeAddress newAddressFull1() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Bukit Cinere");
        result.setSubDistrict("Gandul");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull2() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Melati");
        result.setSubDistrict("Gandul");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull3() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. H. Saleh");
        result.setSubDistrict("Pondok Labu");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull4() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Cilobak");
        result.setSubDistrict("Pondok Labu");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull5() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. PLN");
        result.setSubDistrict("Limo");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull6() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Kecapi");
        result.setSubDistrict("Jagakarsa");
        result.setDistrict("Ciganjur");
        result.setCity("Jakarta Selatan");
        result.setProvince("DKI Jakarta");
        return result;
    }

    public CafeAddress newAddressFull7() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Pertamina");
        result.setSubDistrict("Jagakarsa");
        result.setDistrict("Ciganjur");
        result.setCity("Jakarta Selatan");
        result.setProvince("DKI Jakarta");
        return result;
    }

    public CafeAddress newAddressFull8() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Kukusan");
        result.setSubDistrict("Jagakarsa");
        result.setDistrict("Ciganjur");
        result.setCity("Jakarta Selatan");
        result.setProvince("DKI Jakarta");
        return result;
    }
    
    public CafeAddress newAddressFull9() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Boegenville");
        result.setSubDistrict("Jagakarsa");
        result.setDistrict("Ciganjur");
        result.setCity("Jakarta Selatan");
        result.setProvince("DKI Jakarta");
        return result;
    }

    public CafeAddress newAddressFull10() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Cempaka");
        result.setSubDistrict("Jagakarsa");
        result.setDistrict("Ciganjur");
        result.setCity("Jakarta Selatan");
        result.setProvince("DKI Jakarta");
        return result;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        serviceTested = new CafeAllTimeImpl(twitter4JAdapterImplMock, cafeDatabaseAdapter);
    }

    @Test
    public void showAllTimeSuccessfulTest() {
        CafeId cafeId1 = newCafeIdF1();
        CafeAddress addressFull1 = newAddressFull1();
        Cafe[] cafeOnDatabase = new Cafe[10];
        Cafe cafe1 = new Cafe(cafeId1, addressFull1);
        cafe1.setCafeOfAllTimeRank(1);
        cafeOnDatabase[0] = cafe1;

        CafeId cafeId2 = newCafeIdF2();
        CafeAddress addressFull2 = newAddressFull2();
        Cafe cafe2 = new Cafe(cafeId2, addressFull2);
        cafe2.setCafeOfAllTimeRank(2);
        cafeOnDatabase[1] = cafe2;

        CafeId cafeId3 = newCafeIdF3();
        CafeAddress addressFull3 = newAddressFull3();
        Cafe cafe3 = new Cafe(cafeId3, addressFull3);
        cafe3.setCafeOfAllTimeRank(3);
        cafeOnDatabase[2] = cafe3;

        CafeId cafeId4 = newCafeIdF4();
        CafeAddress addressFull4 = newAddressFull4();
        Cafe cafe4 = new Cafe(cafeId4, addressFull4);
        cafe4.setCafeOfAllTimeRank(4);
        cafeOnDatabase[3] = cafe4;

        CafeId cafeId5 = newCafeIdF5();
        CafeAddress addressFull5 = newAddressFull5();
        Cafe cafe5 = new Cafe(cafeId5, addressFull5);
        cafe5.setCafeOfAllTimeRank(5);
        cafeOnDatabase[4] = cafe5;

        CafeId cafeId6 = newCafeIdF6();
        CafeAddress addressFull6 = newAddressFull6();
        Cafe cafe6 = new Cafe(cafeId6, addressFull6);
        cafe6.setCafeOfAllTimeRank(6);
        cafeOnDatabase[5] = cafe6;

        CafeId cafeId7 = newCafeIdF7();
        CafeAddress addressFull7 = newAddressFull7();
        Cafe cafe7 = new Cafe(cafeId7, addressFull7);
        cafe7.setCafeOfAllTimeRank(7);
        cafeOnDatabase[6] = cafe7;

        CafeId cafeId8 = newCafeIdF8();
        CafeAddress addressFull8 = newAddressFull8();
        Cafe cafe8 = new Cafe(cafeId8, addressFull8);
        cafe8.setCafeOfAllTimeRank(8);
        cafeOnDatabase[7] = cafe8;

        CafeId cafeId9 = newCafeIdF9();
        CafeAddress addressFull9 = newAddressFull9();
        Cafe cafe9 = new Cafe(cafeId9, addressFull9);
        cafe9.setCafeOfAllTimeRank(9);
        cafeOnDatabase[8] = cafe9;

        CafeId cafeId10 = newCafeIdF10();
        CafeAddress addressFull10 = newAddressFull10();
        Cafe cafe10 = new Cafe(cafeId10, addressFull10);
        cafe10.setCafeOfAllTimeRank(10);
        cafeOnDatabase[9] = cafe10;
        
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();
        serviceTested.showAllTime(123);
        //verify(cafeDatabaseAdapter).getAllCafe();
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Here is out top 10 cafe of all time:\n"
                        .concat("\nBlue Bottle\nAddress: Jl. Bukit Cinere, Gandul, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 1\nCafe of The Week Wins: 0\n")
                        .concat("\nFore\nAddress: Jl. Melati, Gandul, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 2\nCafe of The Week Wins: 0\n")
                        .concat("\nStarbucks\nAddress: Jl. H. Saleh, Pondok Labu, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 3\nCafe of The Week Wins: 0\n")
                        .concat("\nMaxx\nAddress: Jl. Cilobak, Pondok Labu, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 4\nCafe of The Week Wins: 0\n")
                        .concat("\nKitchenette\nAddress: Jl. PLN, Limo, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 5\nCafe of The Week Wins: 0\n")
                        .concat("\nKoi\nAddress: Jl. Kecapi, Jagakarsa, Ciganjur, Jakarta Selatan, DKI Jakarta\n")
                        .concat("Cafe Rank: 6\nCafe of The Week Wins: 0\n")
                        .concat("\nWarkop 96\n")
                        .concat("Address: Jl. Pertamina, Jagakarsa, Ciganjur, Jakarta Selatan, DKI Jakarta\n")
                        .concat("Cafe Rank: 7\nCafe of The Week Wins: 0\n")
                        .concat("\nWarkop 97\n")
                        .concat("Address: Jl. Kukusan, Jagakarsa, Ciganjur, Jakarta Selatan, DKI Jakarta\n")
                        .concat("Cafe Rank: 8\nCafe of The Week Wins: 0\n")                        
                        .concat("\nWarkop 98\n")
                        .concat("Address: Jl. Boegenville, Jagakarsa, Ciganjur, Jakarta Selatan, DKI Jakarta\n")
                        .concat("Cafe Rank: 9\nCafe of The Week Wins: 0\n")                        
                        .concat("\nWarkop 99\n")
                        .concat("Address: Jl. Cempaka, Jagakarsa, Ciganjur, Jakarta Selatan, DKI Jakarta\n")
                        .concat("Cafe Rank: 10\n")
                        .concat("Cafe of The Week Wins: 0\n")                                                
        );
    }
}
