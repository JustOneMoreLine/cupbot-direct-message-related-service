package com.cupbotteam.directmessagerelatedservice.service.apps;

import com.cupbotteam.directmessagerelatedservice.core.Cafe;
import com.cupbotteam.directmessagerelatedservice.core.CafeAddress;
import com.cupbotteam.directmessagerelatedservice.core.CafeId;
import com.cupbotteam.directmessagerelatedservice.service.CafeDatabaseAdapter;
import com.cupbotteam.directmessagerelatedservice.service.Twitter4JAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class CafeNearbyTest {
    @Mock
    Twitter4JAdapter twitter4JAdapterImplMock;
    @Spy
    CafeDatabaseAdapter cafeDatabaseAdapter;
    @InjectMocks
    CafeNearbyImpl serviceTested;
    @Captor
    ArgumentCaptor argumentCaptor;

    public CafeId newCafeIdF1() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Blue Bottle");
        return result;
    }

    public CafeId newCafeIdF2() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Fore");
        return result;
    }

    public CafeId newCafeIdF3() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Starbucks");
        return result;
    }

    public CafeId newCafeIdF4() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Maxx");
        return result;
    }

    public CafeId newCafeIdF5() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Kitchenette");
        return result;
    }

    public CafeId newCafeIdL1() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Koi");
        return result;
    }

    public CafeId newCafeIdL2() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Warkop 96");
        return result;
    }

    public CafeId newCafeNone() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("Nonexistent");
        return result;
    }

    public CafeAddress newAddressFull1() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Bukit Cinere");
        result.setSubDistrict("Gandul");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull2() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Melati");
        result.setSubDistrict("Gandul");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull3() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. H. Saleh");
        result.setSubDistrict("Pondok Labu");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull4() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Cilobak");
        result.setSubDistrict("Pondok Labu");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressFull5() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. PLN");
        result.setSubDistrict("Limo");
        result.setDistrict("Cinere");
        result.setCity("Depok");
        result.setProvince("Jawa Barat");
        return result;
    }

    public CafeAddress newAddressL1() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Kecapi");
        result.setSubDistrict("Jagakarsa");
        result.setDistrict("Ciganjur");
        result.setCity("Jakarta Selatan");
        result.setProvince("DKI Jakarta");
        return result;
    }

    public CafeAddress newAddressL2() {
        CafeAddress result = new CafeAddress();
        result.setStreet("Jl. Pertamina");
        result.setSubDistrict("Jagakarsa");
        result.setDistrict("Ciganjur");
        result.setCity("Jakarta Selatan");
        result.setProvince("DKI Jakarta");
        return result;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        serviceTested = new CafeNearbyImpl(twitter4JAdapterImplMock, cafeDatabaseAdapter);
    }

    @Test
    public void showBestFiveSuccessfulTest() {
        CafeId cafeId1 = newCafeIdF1();
        CafeAddress addressFull1 = newAddressFull1();
        Cafe[] cafeOnDatabase = new Cafe[5];
        Cafe cafe1 = new Cafe(cafeId1, addressFull1);
        cafe1.setCafeOfAllTimeRank(1);
        cafeOnDatabase[0] = cafe1;

        CafeId cafeId2 = newCafeIdF2();
        CafeAddress addressFull2 = newAddressFull2();
        Cafe cafe2 = new Cafe(cafeId2, addressFull2);
        cafe2.setCafeOfAllTimeRank(2);
        cafeOnDatabase[1] = cafe2;

        CafeId cafeId3 = newCafeIdF3();
        CafeAddress addressFull3 = newAddressFull3();
        Cafe cafe3 = new Cafe(cafeId3, addressFull3);
        cafe3.setCafeOfAllTimeRank(3);
        cafeOnDatabase[2] = cafe3;

        CafeId cafeId4 = newCafeIdF4();
        CafeAddress addressFull4 = newAddressFull4();
        Cafe cafe4 = new Cafe(cafeId4, addressFull4);
        cafe4.setCafeOfAllTimeRank(4);
        cafeOnDatabase[3] = cafe4;

        CafeId cafeId5 = newCafeIdF5();
        CafeAddress addressFull5 = newAddressFull5();
        Cafe cafe5 = new Cafe(cafeId5, addressFull5);
        cafe5.setCafeOfAllTimeRank(5);
        cafeOnDatabase[4] = cafe5;

        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();
        serviceTested.showNearby(123, "Cinere", "Depok", "Jawa Barat");
        //verify(cafeDatabaseAdapter).getAllCafe();
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "We have found the best cafes in your area! Here are they:\n"
                        .concat("\nBlue Bottle\nAddress: Jl. Bukit Cinere, Gandul, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 1\nCafe of The Week Wins: 0\n")
                        .concat("\nFore\nAddress: Jl. Melati, Gandul, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 2\nCafe of The Week Wins: 0\n")
                        .concat("\nStarbucks\nAddress: Jl. H. Saleh, Pondok Labu, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 3\nCafe of The Week Wins: 0\n")
                        .concat("\nMaxx\nAddress: Jl. Cilobak, Pondok Labu, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 4\nCafe of The Week Wins: 0\n")
                        .concat("\nKitchenette\nAddress: Jl. PLN, Limo, Cinere, Depok, Jawa Barat\n")
                        .concat("Cafe Rank: 5\nCafe of The Week Wins: 0\n")
        );
    }

    @Test
    public void showBestCafesInListSuccessfulTest() {
        CafeId cafeId1 = newCafeIdL1();
        CafeAddress address1 = newAddressL1();
        Cafe[] cafeOnDatabase = new Cafe[2];
        Cafe cafe1 = new Cafe(cafeId1, address1);
        cafe1.setCafeOfAllTimeRank(11);
        cafeOnDatabase[0] = cafe1;

        CafeId cafeId2 = newCafeIdL2();
        CafeAddress address2 = newAddressL2();
        Cafe cafe2 = new Cafe(cafeId2, address2);
        cafe2.setCafeOfAllTimeRank(12);
        cafeOnDatabase[1] = cafe2;

        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();
        serviceTested.showNearby(123, "Ciganjur", "Jakarta Selatan", "DKI Jakarta");
        //verify(cafeDatabaseAdapter).getAllCafe();
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "We have found the best cafes in your area! Here are they:\n"
                        .concat("\nKoi\nAddress: Jl. Kecapi, Jagakarsa, Ciganjur, Jakarta Selatan, DKI Jakarta\n")
                        .concat("Cafe Rank: 11\nCafe of The Week Wins: 0\n")
                        .concat("\nWarkop 96\n")
                        .concat("Address: Jl. Pertamina, Jagakarsa, Ciganjur, Jakarta Selatan, DKI Jakarta\n")
                        .concat("Cafe Rank: 12\nCafe of The Week Wins: 0\n")
        );
    }

    @Test
    public void noCafeFoundTest() {
        CafeId none = newCafeNone();
        Cafe[] cafeOnDatabase = new Cafe[0];
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();
        serviceTested.showNearby(123, "Manhattan", "NYC", "NY");
        verify(cafeDatabaseAdapter).getAllCafe();
        verify(twitter4JAdapterImplMock).sendDirectMessage(123,
                "Sorry, we could not find any cafes anywhere near your location :(");
    }
}
