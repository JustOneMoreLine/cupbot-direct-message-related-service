package com.cupbotteam.directmessagerelatedservice.service;

import com.cupbotteam.directmessagerelatedservice.utility.Sha256;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class AuthenticationImplTest {
    Authentication auth;

    @BeforeEach
    public void setAuth() {
        auth = new AuthenticationImpl();
    }

    @Test
    public void testGenerateCrcToken() {
        // test for generating "A" token
        String crcToken1 = auth.generateCrcToken();
        assertEquals(crcToken1.length(), 10);

        // test for generating "B" token
        String crcToken2 = auth.generateCrcToken();
        assertEquals(crcToken2.length(), 10);

        // test if both generated token are different
        assertNotEquals(crcToken1, crcToken2);
    }

    @Test
    public void testAuthenticate() {
        // set up
        String crcToken1 = auth.generateCrcToken();
        String response = Sha256.hash(crcToken1);

        // test
        assertTrue(auth.authenticateResponseToken(response));

        // set up 2
        String crcToken2 = auth.generateCrcToken();

        // test 2
        assertFalse(auth.authenticateResponseToken(crcToken2));
    }

    @Test
    public void testRemoveOutdatedTokens() {
        // set up
        auth.addGeneratedTokens("ABCDE", LocalDateTime.now());
        auth.addGeneratedTokens("FGHIJ", LocalDateTime.now().minusHours(2));
        auth.checkOutdateTokens();

        // test
        assertTrue(auth.isTokenStillValid("ABCDE"));
        assertFalse(auth.isTokenStillValid("FGHIJ"));

        // clean up
        auth.removeGeneratedTokens("ABCDE");
        auth.removeGeneratedTokens("FGHIJ");
    }
}
