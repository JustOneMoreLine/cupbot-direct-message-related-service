package com.cupbotteam.directmessagerelatedservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import twitter4j.AsyncTwitter;
import twitter4j.Twitter;
import static org.mockito.Mockito.verify;

public class Twitter4JAdapterImplTest {
    Twitter4JAdapter twitter4JAdapter;
    @Spy
    Twitter twitter;
    @Spy
    AsyncTwitter asyncTwitter;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        twitter4JAdapter = new Twitter4JAdapterImpl(twitter, asyncTwitter);
    }

    @Test
    public void testCreateFavorite() {
        twitter4JAdapter.createFavorite(123);
        try {
            verify(twitter).createFavorite(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testDestroyFavorite() {
        twitter4JAdapter.destroyFavorite(123);
        try {
            verify(twitter).destroyFavorite(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testRetweetStatus() {
        twitter4JAdapter.retweetStatus(123);
        try {
            verify(twitter).retweetStatus(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testUnRetweetStatus() {
        twitter4JAdapter.unRetweetStatus(123);
        try {
            verify(twitter).unRetweetStatus(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testShowStatus() {
        twitter4JAdapter.showStatus(123);
        try {
            verify(twitter).showStatus(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testDestroyStatus() {
        twitter4JAdapter.destroyStatus(123);
        try {
            verify(twitter).destroyStatus(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testUpdateStatus() {
        twitter4JAdapter.updateStatus("ABCDE");
        try {
            verify(asyncTwitter).updateStatus("ABCDE");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testShowDirectMessage() {
        twitter4JAdapter.showDirectMessage(123);
        try {
            verify(twitter).showDirectMessage(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void testDestroyDirectMessage() {
        twitter4JAdapter.destroyDirectMessage(123);
        try {
            verify(twitter).destroyDirectMessage(123);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void sendDirectMessage() {
        twitter4JAdapter.sendDirectMessage(123, "ABCDE");
        verify(asyncTwitter).sendDirectMessage(123, "ABCDE");
    }

    @Test
    public void getRateLimitStatus() {
        twitter4JAdapter.getRateLimitStatus();
        try {
            verify(twitter).getRateLimitStatus();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
