package com.cupbotteam.directmessagerelatedservice.controller;

import com.cupbotteam.directmessagerelatedservice.core.DirectMessageJson;
import com.cupbotteam.directmessagerelatedservice.service.Authentication;
import com.cupbotteam.directmessagerelatedservice.service.keyword.KeywordHandlerController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class DirectMessageRelatedServiceControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private Authentication authentication;
    @MockBean
    private KeywordHandlerController keywordHandlerController;

    public String exampleStringDirectMessageJson() {
        String result = "";
        DirectMessageJson directMessageJson = new DirectMessageJson();
        directMessageJson.setDmId(123);
        directMessageJson.setReceiverID(456);
        directMessageJson.setSenderID(789);
        directMessageJson.setText("AHAHAHAHA");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        try {
            result = ow.writeValueAsString(directMessageJson);
        } catch (JsonProcessingException e) {
            System.out.println(e);
        }
        return result;
    }

    public DirectMessageJson exampleDirectMessageJson() {
        DirectMessageJson result = new DirectMessageJson();
        result.setDmId(123);
        result.setReceiverID(456);
        result.setSenderID(789);
        result.setText("AHAHAHAHA");
        return result;
    }

    @Test
    public void testWakeMeUp() throws Exception {
        mockMvc.perform(get("/wakeMe")).andExpect(status().isOk());
    }

    @Test
    public void testChallange() throws Exception {
        mockMvc.perform(get("/challange")).andExpect(status().isOk());
        verify(authentication).generateCrcToken();
    }

    @Test void testAuthenticate() throws Exception {
        mockMvc.perform(post("/webhook")
                .contentType(MediaType.APPLICATION_JSON)
                .content(exampleStringDirectMessageJson())
                .param("response", "responsetoken"));
        verify(authentication).authenticateResponseToken("responsetoken");
    }

    @Test
    public void testWebhookSuccess() throws Exception {
        doReturn(true).when(authentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(post("/webhook")
                .contentType(MediaType.APPLICATION_JSON)
                .content(exampleStringDirectMessageJson())
                .param("response", "responsetoken"))
                .andExpect(status().isOk());
        verify(keywordHandlerController).handle(any(DirectMessageJson.class));
    }

    @Test
    public void testWebhookFail() throws Exception {
        doReturn(false).when(authentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(post("/webhook")
                .contentType(MediaType.APPLICATION_JSON)
                .content(exampleStringDirectMessageJson())
                .param("response", "responsetoken"))
                .andExpect(status().isForbidden());
        verify(keywordHandlerController, never()).handle(any(DirectMessageJson.class));
    }

    @Test
    public void testAsyncWebhookSuccess() throws Exception {
        doReturn(true).when(authentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(post("/asyncWebhook")
                .contentType(MediaType.APPLICATION_JSON)
                .content(exampleStringDirectMessageJson())
                .param("response", "responsetoken"))
                .andExpect(status().isOk());
        Thread.sleep(5000);
        verify(keywordHandlerController).handle(any(DirectMessageJson.class));
    }

    @Test
    public void testAsyncWebhookFail() throws Exception {
        doReturn(false).when(authentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(post("/asyncWebhook")
                .contentType(MediaType.APPLICATION_JSON)
                .content(exampleStringDirectMessageJson())
                .param("response", "responsetoken"))
                .andExpect(status().isOk());
        Thread.sleep(5000);
        verify(keywordHandlerController, never()).handle(any(DirectMessageJson.class));
    }
}
