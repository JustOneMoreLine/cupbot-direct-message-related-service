Master:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/master/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/master)
[![coverage report](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/master/coverage.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/master)

Project Base:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/project_base/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/project_base)
[![coverage report](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/project_base/coverage.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/project_base)

Cafe Registration:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/cafeRegistration/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/cafeRegistration)
[![coverage report](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/cafeRegistration/coverage.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/cafeRegistration)

Cafe with Promo:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/dana/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/dana)
[![coverage report](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/dana/coverage.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/dana)

Cafe Nearby:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/kirana/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/kirana)
[![coverage report](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/kirana/coverage.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/kirana)

Cafe of All Time:
[![pipeline status](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/mario/pipeline.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/mario)
[![coverage report](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/badges/mario/coverage.svg)](https://gitlab.com/JustOneMoreLine/cupbot-direct-message-related-service/-/commits/mario)